% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/prettyP.R
\name{prettyP}
\alias{prettyP}
\title{Pretty P-Values}
\usage{
prettyP(
  p.value,
  ndigit = 3,
  sig.level = 0.05,
  min.display.level = 0.001,
  vickers = F
)
}
\arguments{
\item{p.value}{p-value (numeric).}

\item{ndigit}{number of digit; the default is 3. It can also take sprintf format.}

\item{sig.level}{significant level to add asterisk; the default is 0.05.}

\item{min.display.level}{minimal display level; the default is 0.001 (i.e., <0.001).}

\item{vickers}{logistic variable; Use Vickers format or not; the default is F.}
}
\description{
Pretty up p-values
}
\details{
A function to pretty up p-values for output tables
}
\section{Version control}{

\itemize{
\itemize{
\item Created - 11/18/2016 Rebecca Huang (RH);
\item Updated - \itemize{
\itemize{
\item 11/18/2016 (RH); 11/22/2016 (RH);
\item 03/31/2017 (RH) - sprintf format;
\item 09/06/2017 (DD) - Add vickers formart of p-value; signif format;
}
}
}
}
}

\examples{
\dontrun{
prettyP(c(0.03, 0.001, 0.4))

prettyP(0.03, ndigit = "\%.3g")
}
}
\seealso{
\itemize{
\itemize{
\item \code{\link[gdxBiostats1]{prettyRout}} for format R output to be "paste-friendly"
\item \code{\link[gdxBiostats1]{numDecimal}} for counting the number of decimal digits
}
}

Other Utilities: 
\code{\link{addAlpha}()},
\code{\link{cleanColnames}()},
\code{\link{getDecipherCol}()},
\code{\link{makeFootnote}()},
\code{\link{numDecimal}()},
\code{\link{prettyROut}()},
\code{\link{renameGdx}()},
\code{\link{reorderLevel}()},
\code{\link{smartFileSave}()},
\code{\link{smartSummary}()},
\code{\link{tryCatchFunc}()}
}
\concept{Utilities}
