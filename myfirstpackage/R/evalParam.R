#'
#' Evaluate Paramaters
#'
#' Function to assign parameters their argmuents from a given string of parameters and their corresponding
#' arguments
#'
#' Last updated 11/24/2016 (RH); 07/06/2017 (LL, RH); 10/19/2017 (RH);
#'
#' @param myString a string that contains a list of parameters with their assignments (i.e., the equal sign) of arguments; parameter names requires to only be a combination of characters, numbers, underscores and periods without any spacing. A useful tip, if any of the arguments contains string that is wrapped by double quotations then myString should be wrapped by single quotations and vice versa.
#' @param rmFromGlobalEnv remove from Glabal environment or not? The default is not.
#'
#' @section Version control:
#' \itemize{
#' * Created 11/24/2016 (RH);
#' * Updated - \itemize{
#' * 07/06/2017 (LL, RH); 10/19/2017 (RH);
#'   }
#' }
#'
#' @family Utilities
#'
#' @examples
#' \dontrun{
#'
#' # For example, if a function we would like to debug looks like this: myFunction <- function(a.1 = list(c(1, 2), c(2, 3))){...}
#'
#' myString <- 'a.1 = list(c(1, 2), c(2, 3))'
#' evalParam(myString)
#' evalParam(myString, rmFromGlobalEnv = TRUE)
#'
#' # For example, if a function we would like to debug looks like this:
#' #    myFunction <- function(a.2 = "A", bda134  = "B", c098DF = c(1, 2, 4), d = list(c(1, 2), c(2, 3)), e = c(1, 3, 4)){...}
#'
#' myString <- 'a.2 = "A", bda134  = "B", c098DF = c(1, 2, 4), d = list(c(1, 2), c(2, 3)), e = c(1, 3, 4)'
#' evalParam(myString)
#' evalParam(myString, rmFromGlobalEnv = TRUE)
#'
#' # For example, if a function we would like to debug looks like this:
#' #    myFunction <- function(a = "A", b  = "B", c = c(1, 2, 4),
#' #                           d = list(c(1, 2), c(2, 3)),
#' #                           e = c(1, 3, 4)){...}
#'
#' myString <- 'a = "A", b  = "B", c = c(1, 2, 4),
#'   d = list(c(1, 2), c(2, 3)),
#'   e = c(1, 3, 4)'
#' evalParam(myString)
#' evalParam(myString, rmFromGlobalEnv = TRUE)
#'
#' }
#' @export
#'

"evalParam" <- function(myString, rmFromGlobalEnv = FALSE){

  library(stringr)

  var.names <- str_extract_all(myString, "[a-zA-Z0-9\\.\\_]* *\\= *")[[1]]

  var.values <- sapply(1:length(var.names), function(i){
    target <- var.names[[i]]
    if(i == length(var.names)){
      next.target <- "$"
    } else {
      next.target <- var.names[[I(i+1)]]
    }
    values <- gsub(sprintf(".*%s(.*?)%s.*", target, next.target), "\\1", myString)
    values <- gsub(",[ \n]*$", "", values) # remove ending ,
    # values <- gsub(",\n*", "", values)
    return(values)
  })

  results <- paste0(var.names, var.values)

  if(!rmFromGlobalEnv){

    eval(parse(text = results), envir = .GlobalEnv)
    return(results)

  } else{

    rm(list = ls(envir = .GlobalEnv)[ls(envir = .GlobalEnv) %in% trimws(gsub("=", "", var.names))], envir = .GlobalEnv)

  }

}



