
#' Genomic Charactieristics Tables
#'
#' Functions to create genomic characteristics table then turn them into flextables
#'
#' \code{genoCharTable()} automagically generates  "Table 1" for your clinical paper with
#' or without groups of stratification and with the option to report test p-values. For best use of this function,
#' pre-format your data.frame to have the group labels you want. The levels contain the order you want
#'  in the table and labels are, well, the labels
#'
#' Can use \code{patient_char_table_flex()} to renames the variables to be
#' more presentable, add new header row to the table, and return a zebra-style flextable
#'
#' @param df a data frame including the genomic signature/biomarker variables to be reported and the groupVar varaible for the column split if applicable
#' @param varList a vector of variable list for the genomic signature/biomarker variables to be reported
#' @param catVarList a vector of variable from the varList that should be treated as categorical
#' @param groupVar a variable that the genomic summary is wished to be reported by (i.e., grouped by results)
#' @param renameList a list of renames specified for renameGdx(); e.g., '"decipher_1" = "Decipher score", "decipherv2_2" = "Decipher v2 score"'
#' @param iCompare show p-value from comparative analyses or not if groupVar is specified; the default is iCompare = FALSE
#' @param dropLevelList a list of regex OR condition to pick out levels to drop from the reported table (make sure not to accidentally drop the actual variable); a default list already provided
#' @param catPerc number of decimal places to display for percentages
#' @param contDig continuous summary statistics, respectively, in the format used for sprintf a.k.a. the C-style C99 standard
#' @param contSummType choose one or more ways to summarise continuous variables (default is all 4)
#' @param catUseNA logical value: do you want to display counts for the number of missing values in a categorical variable? (Default is "TRUE")
#' @param contUseNA logical value: do you want to display counts for the number of missing values in a continuous variable? (Default is "TRUE")
#' @param size font size (Default is 8)
#' @param headerNames rename header (Default = NULL)
#'
#' @return \code{flextable}  presenting the results of the genomic summary
#'
#' @section Version control:
#' \itemize{
#'  * Created - 02/25/2020 (RH)
#'  * Updated - \itemize{
#'  * 04/14/2020 (VL) - Updated to prevent dropping levels that are wanted but have at
#'                               least one NA in the duplicated occurences
#'  * 05/21/2020 (RH) - add header name and check whether groupVar is used in varList
#'  * 06/29/2020 (RH) - add font size
#'    }
#' }
#'
#' @family Tables
#' @seealso
#' \itemize{
#' * \code{\link[gdxBiostats1]{patientCharTable}} for the Patients Characteristics Table
#' * \code{\link[gdxBiostats1]{patientCharTableFlex}} for the function to turn patChar and genoChar tables into flextabls
#' }
#'
#' @examples
#' \dontrun{
#' library(dplyr)
#'
#' renameList <- '
#' "aros_1" = "AR-A",
#' "pam50_1" = "PAM50"'
#'
#' set.seed(64)
#' df <- data.frame(cohort = rep(LETTERS[1:2], 5),
#'                  decipher_1 = rnorm(n = 10),
#'                  pam50_1 = sample(c("Basal", "LuminalA", "LuminalB"), size = 10, replace = TRUE),
#'                  aros_1 = sample(c("Average AR Activity", "Lower AR Activity"), size = 10, replace = TRUE)) %>%
#'   mutate(decipher_1_cat = ifelse(decipher_1 < 0.45, "Low", "High"))
#'
#' sig.vars <- c("decipher_1", "decipher_1_cat", "aros_1", "pam50_1")
#' sig.vars.cat <- c("decipher_1_cat", "aros_1", "pam50_1")
#'
#' (Ftab.sig.bycohort <- genoCharTable(df = df,
#'                                     varList = sig.vars,
#'                                     catVarList = sig.vars.cat,
#'                                     groupVar = "cohort",
#'                                     dropLevelList = "Average AR Activity",
#'                                     renameList = renameList,
#'                                     iCompare = TRUE) %>%
#'     width(j = 1:length(.$col_keys), width = c(2, rep(1.2, length(.$col_keys) - 1))))
#' }
#'
#' @export
#'

"genoCharTable" <- function(df, varList, catVarList,
                            groupVar = "cohort",
                            catPerc = ".1f",
                            contDig = ".2g",
                            renameList = NULL,
                            iCompare = FALSE,
                            contSummType = "medQ1Q3",
                            catUseNA =  TRUE,
                            contUseNA = TRUE,
                            headerNames = NULL,
                            size = 8,
                            dropLevelList = "ERG negative|Average ADT Response|Average AR Activity|No RB-loss|PTEN Normal|Adenocarcinoma"){

  varList <- varList[!varList %in% groupVar]
  tab <- patient_char_table(df = df, var = varList,
                            group.var = groupVar,
                            cat.perc = catPerc,
                            cont.dig = contDig,
                            cat.id = which(varList %in% catVarList),
                            cat.useNA = catUseNA, cont.useNA = contUseNA,
                            cont.summ.type = contSummType, test = iCompare)

  temp <- tab$Variables[which(rowSums(is.na(tab)) == (ncol(tab) - 1))]
  tab <- tab[!tab$Variables %in% temp[which(duplicated(gsub("_cat|_quartile", "", temp)))], ]
  tab[1, ] <- gsub(" \\(.*", "", tab[1, ])
  if(length(grep(dropLevelList, tab$Variables)) != 0){
    tab <- tab[-grep(dropLevelList, tab$Variables), ]
  }

  if(is.null(headerNames)) headerNames <- colnames(tab)

  Ftab <- patient_char_table_flex(tab, pmerge = iCompare, zebra = FALSE,
                                  newRenameList = renameList, size = size,
                                  header.names = gsub("^p$", "P-value",
                                                      headerNames)) %>%
    fix_border_issues()

  rm(tab, temp)

  return(Ftab)

}
