
#'
#' CAPRA-S Score
#'
#' Function to generate Cancer of the Prostate Risk Assessment post-Surgical (CAPRA-S) scores based on clinical and
#' pathological variables from patients undergoing radical prostatectomy (RP)
#'
#' The CAPRA-S score is a straightforward 0 to 12 score. The score is calculated using points assigned to: pre-surgical PSA,
#' Gleason score, surgical margins, ECE, SVI, and LNI.A CAPRA score of 0 to 2 indicates low-risk, 3 to 5 indicates
#' intermediate-risk, and 6 to 10 indicates high-risk. The function is tailored for database use for both
#' retrospective data and commercial data.
#'
#' Note: bx_pct_pos_cores.fld will assume that %-value (i.e. from 0-100). If if is not in this format,
#'   use the bx.pct option and switch to FALSE, and this will assume a 0-1 value.
#'
#' @param df data.frame containing necessary variables to compute CAPRA-S score
#' @param pathgs_p.fld,pathgs_s.fld names of fields containing primary and secondary pathological Gleason scores, respectively
#' @param epe.fld,svi.fld,sm.fld,lni.fld names of fields in df containing EPE, SVI, SM, and LNI status (should be binary in nature or else you might get weird results)
#' @param epe.vals,svi.vals,sm.vals,lni.vals vectors of length two containing the values corresponing to negative and positive status of the respective fields. By default, it is numeric with values 0 and 1, but it can be a character or logical vector. Any value found in the data that does not match either of these values will result in a value of NA
#'
#' @return numeric vector of CAPRA-S scores
#'
#' @section Version control:
#' \itemize{
#' * Created - 12/20/2016 (ZH, NF)
#' * Updated - \itemize{
#' * 2016 - (NF) allows for input field names rather than fixed names from db; allows for vector addition by replacing car.recode
#' method with cut method; allows for different values of EPE, SVI, SM, and LNI (but defaults are numeric 0,1)}
#' }
#'
#' @family Nomograms
#' @seealso
#' \itemize{
#' * \code{\link[gdxBiostats1]{capra}} for generating the CAPRA score
#' }
#'
#' @references
#' https://prostatecancerinfolink.net/2011/06/29/capra-s-scores-and-projection-of-prostate-cancer-recurrence-post-surgery/
#'
#' @examples
#' \dontrun{
#' data.meta <- dplyr::mutate(data.meta, capra_s = capraS(data.meta))
#' }
#' @export

"capraS" <- function(df, preop_psa.fld = 'preop_psa', pathgs.fld = 'pathgs',
                  pathgs_p.fld = 'pathgs_p', pathgs_s.fld = 'pathgs_s',
                  epe.fld = 'epe', epe.vals = c(no = 0, yes = 1),
                  svi.fld = 'svi', svi.vals = c(no = 0, yes = 1),
                  sm.fld = 'sm', sm.vals = c(no = 0, yes = 1),
                  lni.fld = 'lni', lni.vals = c(no = 0, yes = 1)){
  stopifnot(all(c(preop_psa.fld, pathgs.fld, pathgs_p.fld, pathgs_s.fld, epe.fld, svi.fld,
                  sm.fld, lni.fld) %in% names(df)),
            all(length(svi.vals) == 2, length(lni.vals) == 2,
                length(epe.vals) == 2, length(sm.vals) == 2))

  fun <- function(x) as.numeric(as.character(x))

  pPSA <- fun(cut(df[[preop_psa.fld]], c(-Inf, 6, 10, 20, Inf), 0:3))
  gsp <- df[[pathgs_p.fld]]; gss <- df[[pathgs_s.fld]]
  gs <- ifelse((is.na(gsp) | is.na(gss)), df[[pathgs.fld]], gsp+gss)
  bGS <- (gs >= 7) + ((gsp >= 4 & gs >= 7) | (gs >= 8)) + (gs >= 8)
  SM <- 2*(match(df[[sm.fld]], sm.vals) - 1)
  SVI <- 2*(match(df[[svi.fld]], svi.vals) - 1)
  EPE <- match(df[[epe.fld]], epe.vals) - 1
  LNI <- match(df[[lni.fld]], lni.vals) - 1

  pPSA + bGS + SM + SVI + EPE + LNI

}

