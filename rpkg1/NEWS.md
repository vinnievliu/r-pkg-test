## version 0.0.0.9002

---


### FxnUpdates

- Added RH's updates to functions: plotCumInc, simpleModelFlex, and coxUVAMVA
- 2020-08-07 12:22:41


## version 0.0.0.9001

---


### DescEdit

- Added Rebecca to auth list
- Testing news.md


## version 0.0.0.9000

---

### NEWS.md setup

- added NEWS.md creation with newsmd

