
#'
#' mskNomogram
#'
#' Function that calculates the MSK Nomogram
#'
#' Based on biopsy clinical and pathological informaton, prediction of EPE, SVI, LNI, and OC (organ-confined disease).
#' Based on publically available model provided in References. This function has coded the subset of logistic nomograms.
#'
#' @param df data.frame containing necessary variables with which to calculate NCCN risk score
#' @param option a character value either "oc", "epe", "svi", or "lni" for endpoint probability to predict
#' @param psa.fld field of df containing PSA values (ng/mL). Nomogram built for values in 0.1-100, so values outside this range will be converted automatically to the respective range limit.
#' @param clings_p.fld,clings_s.fld,names of fields of df corresponding to Primary and Secondary Gleason scores from biopsy, respectively
#' @param cstage.fld name of field in df corresponding to clinical stage (e.g. T2a,T3). Note, nomogram will not be calculated for T4 disease
#' @param bx_pct_pos_cores.fld,bx_pos_cores.fld,bx_total_cores.fld names of fields of df corresponding to, respectively, number of positive cores and total number of cores from biopsy. These fields are optional, and therefore if NULL or if not found in the data.frame, no cores model will be used.
#'
#' @return predicted probability 0-1 of the endpoint specified in "option" occurring at RP
#'
#' @section Version control:
#' \itemize{
#' * Created - 12/18/2017 (NF)
#' * Updated - \itemize{
#' * }
#' }
#'
#' @family Nomograms
#'
#' @references
#' https://www.mskcc.org/nomograms/prostate/pre_op/coefficients
#'
#' @examples
#' \dontrun{
#' retro.bx <- retro.bx %>%
#'   mutate(lni.msk = mskNomogram(., psa.fld = 'psa'),
#'          svi.msk = mskNomogram(., option = 'svi', psa.fld = 'psa'),
#'          epe.msk = mskNomogram(., option = 'epe', psa.fld = 'psa'),
#'          oc.msk = mskNomogram(., option = 'oc', psa.fld = 'psa'),
#'          svi.msk.nocores = mskNomogram(., option = 'svi', psa.fld = 'psa', bx_pos_cores.fld = NULL),
#'          oc.msk.nocores = mskNomogram(., option = 'oc', psa.fld = 'psa', bx_pos_cores.fld = NULL))
#' }
#' @export

"mskNomogram" <- function(df, option = c('lni', 'svi', 'epe', 'oc'),
                        psa.fld = 'preop_psa',
                        clings_p.fld = 'clings_p', clings_s.fld = 'clings_s',
                        cstage.fld = 'cstage',
                        bx_pos_cores.fld = 'bx_pos_cores', bx_total_cores.fld = 'bx_total_cores'){
  stopifnot(all(c(psa.fld, clings_p.fld, clings_s.fld, cstage.fld) %in% names(df)))

  option <- match.arg(option)

  ## recode df

  ##############
  ## Cstage formatting
  ###############

  # cstage: here we're converting any "Tx" to NA
  cstage <- df[[cstage.fld]]
  ## make cstage "pretty"
  cstage <- tolower(cstage)
  cstage <- gsub("x", "", gsub(".*t", "t", cstage))
  ## anything that was Tx is now just "t", convert to NA:
  cstage[cstage == 't'] <- NA
  ## perform overall check of format:
  if(any(!grepl('t[1234][abc]?', na.omit(cstage)))) stop("Something's wrong with your cstage formatting. Check yo'self 'fore you wreck yo'self \n")
  if(any(grepl('t4', cstage))) warning('Stage T4 cases exist in data. Nomogram will not be calculated for these cases.')
  ## convert to T1, T2a, T2b, T2c, T3+, or NA
  cstage.pts <- (grepl('t(1|2[abc]|3)', cstage)) + (grepl('t(2[abc]|3)', cstage)) +
    (grepl('t(2[bc]|3)', cstage)) + (grepl('t(2c|3)', cstage)) + (grepl('t3', cstage))
  cstage <- c(NA, 'T1', 'T2a', 'T2b', 'T2c', 'T3+')[cstage.pts + 1]
  cstage <- factor(cstage, levels = c('T1', 'T2a', 'T2b', 'T2c', 'T3+'))

  ###############
  ## Gleason & PSA
  ###############
  gsp <- df[[clings_p.fld]]; gss <- df[[clings_s.fld]]
  ## overall format check:
  if(any(!na.omit(gsp) %in% 2:5) | any(!na.omit(gss) %in% 2:5))
    stop('Some of your Primary/Secondary Gleason scores are fishy ><>. They are not in the 2-5 range \n')
  if(any(na.omit(gsp) == 2) | any(na.omit(gss) == 2))
    warning('You have old Gleason scores (2 for primary/secondary, or 4-5 for total). No one does that anymore, that is sooooo 2004. \n')

  pg4 <- gsp > 3
  sg4 <- gss > 3

  ## format check
  psa <- df[[psa.fld]]
  if(any(na.omit(psa) < 0)) stop('You have negative PSA values! WTF!! \n')
  ## PSA must be in 0.1-100 range
  if(any(na.omit(psa) < 0.1 | na.omit(psa) > 100)){
    warning('PSA must be in 0.1-100 range. Now converting outlying values to the extremes of this limit')
    psa[psa < 0.1] <- 0.1
    psa[psa > 100] <- 100
  }

  ###############
  ## Coefficients
  ###############

  ## cores model:
  coef.cores <- switch(option,
                       lni = c(-3.90664548, 0.04648719, 1.85080788, 0.77070173, 0.37888753, 0.57940132, 0.57618944, 0.9105159, 	-0.11228264, 0.05721468),
                       svi = c(-3.64087032, 0.04444051, 1.61342422, 0.79980864, 0.24802096, 0.32708694, 0.61429198, 0.57498157, -0.13962727, 0.07543931),
                       epe = c(-0.47046815, 0.0532227, 0.6593498,	0.40266113, 0.13022831, 0.29508755, 0.28976819, 0.8531101, 	-0.06051063, 0.08833464),
                       oc = c(0.48559304, -0.06135733, -0.75917868, -0.42046589, -0.11674405, -0.3569897, -0.37818337, -0.98212899, 0.06281136, -0.0917364))
  ## no cores model
  coef <- switch(option,
                 lni = c(-4.78603008, 0.04647603, 2.01834524, 0.91667795, 0.54169879, 0.97420613, 1.09028922, 1.59202656),
                 svi = c(-4.47585322, 0.05520405, 1.60366838, 0.85036906, 0.51591678, 0.7556456, 1.10535272, 1.35042904),
                 epe = c(-0.69762059, 0.0489202, 0.73584198, 0.51859291, 0.30909082, 0.6029168, 0.56517529, 1.21028544),
                 oc = c(0.72159898, -0.05572401, -0.8339879, -0.54290901, -0.31781681, -0.66088364, -0.62862475, -1.2687455))


  ###############
  ## make a new data.frame:
  ###############

  dat <- data.frame(psa, pg4, sg4, cstage)

  if(!is.null(bx_pos_cores.fld) & !is.null(bx_total_cores.fld) & all(c(bx_pos_cores.fld, bx_total_cores.fld) %in% names(df))){
    warning('Using positive and negative # cores version of model')

    ###### formatting check for pos and total
    poscores <- df[[bx_pos_cores.fld]]
    if(any(na.omit(poscores) < 0)) stop('You have negative values of # positive cores! Go back and sort your stuff out. \n')
    totcores <- df[[bx_total_cores.fld]]
    if(any(na.omit(totcores - poscores) < 0)) stop('You have total # cores less than # positive cores. This isn\'t Wonderland or the looking glass, so get a grip on reality!')
    negcores <- totcores - poscores

    dat$negcores <- negcores
    dat$poscores <- poscores
    mat <- model.matrix(~psa + pg4 + sg4 + cstage + negcores + poscores, data = dat)

    ## generate probabilities
    log.est <- mat %*% matrix(coef.cores, ncol = 1)
    prob.est <- exp(log.est)/(exp(log.est) + 1)
    prob.out <- rep(NA, nrow(dat))
    prob.out[complete.cases(dat)] <- prob.est

    ## for cases with missing cores, fit no cores model:
    cores.na.ix <- which(is.na(dat$negcores) & complete.cases(dat[1:4]))
    mat.mini <- model.matrix(~psa + pg4 + sg4 + cstage, data = dat[cores.na.ix,])
    log.est <- mat.mini %*% matrix(coef, ncol = 1)
    prob.est <- exp(log.est)/(exp(log.est) + 1)
    prob.out[cores.na.ix] <- prob.est

  } else{
    warning('# Positive Cores and # Total Cores fields not found in data, so using no cores model')

    mat <- model.matrix(~psa + pg4 + sg4 + cstage, data = dat)

    ## generate probabilities
    log.est <- mat %*% matrix(coef, ncol = 1)
    prob.est <- exp(log.est)/(exp(log.est) + 1)
    prob.out <- rep(NA, nrow(dat))
    prob.out[complete.cases(dat)] <- prob.est

  }
  prob.out
}
