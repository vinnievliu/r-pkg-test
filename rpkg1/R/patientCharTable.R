
#' Patient Characteristics Tables
#'
#' Functions to create patient characteristics table then turn them into flextables
#'
#' \code{patientCharTable()} automagically generates  "Table 1" for your clinical paper with
#' or without groups of stratification and with the option to report test p-values. For best use of this function,
#' pre-format your data.frame to have the group labels you want
#' e.g. re-factor your clinical Gleason Score variable to contain instead of "6", "7", etc,
#' to be labeled "Stage 6", "Stage 7" with factor(..., levels = ..., labels = ...)
#' levels contains the order you want in the table and labels are, well, the labels
#'
#' \code{patientCharTableFlex()} re-orders the sections of the pat_char_table as you wish, renames the variables to be
#' more presentable, addes new header row to the table, and returns a zebra-style flextable
#'
#' @param df data.frame containing clinical variables to report on
#' @param var character vector of column names from df. The order will dictate the output
#' @param cat.id integer vector corresponding to the indices in var which represent categorical variables. NB: values do not have to be coded as a character or factor in df to be considered as categorical in this output
#' @param group.var string of column names (can be character/factor) from df. The level of the factor will determine the order of the column in the table
#' @param cat.perc, cont.dig number of decimal places to display for percentages and continuous summary statistics, respectively, in the format used for sprintf a.k.a. the C-style C99 standard
#' @param cat.useNA logical value: do you want to display counts for the number of missing values in a categorical variable? (Default is "TRUE")
#' @param cont.useNA logical value: do you want to display counts for the number of missing values in a continuous variable? (Default is "TRUE")
#' @param unknown.lab string for labeling NA categories (default is "Unavailable")
#' @param cont.summ.type choose one or more ways to summarise continuous variables (default is all 4)
#' @param time.summ.type choose one or more ways to summarise time variables (default is all 3)
#' @param test logical value for whether to display a test p-value, assuming group.var is not NULL (default is FALSE)
#' @param cont.test.type, cat.test.type specify type of test for continuous variables or categorical variables, respectively
#' @param mult.comp TRUE to adjust for multiple comparison; by default is FASLE
#' @param remove.single.level TRUE to remove single level variable; by default is FALSE
#' @param remove.long.cat TRUE to remove categorical variables that have too many levels
#'
#' @return \code{data.frame} representing the patient characteristics table
#'
#' @param patientTable data.frame reptrsenting the patient characteristics table
#' @param pmerge logical value: do you want to display p-values (default is "TRUE")
#' @param new.names new variable names to be displayed in the output flextable
#' @param size size of the font displayed in the outout flextable (default is "10")
#' @param header.names new column names to be displayed in the output flextable
#' @param zebra logical value: do you want to display the output flextable in zebra color style (defalt is "TRUE)
#' @param zebra.colors displayed zebra colors (default is c('#EEEEEE', 'white'))
#' @param borderWidth border width of the table (default is 1)
#' @param newRenameList a list of renames for the variables using renameGDx()
#' @param borderWidth border width; (default is 1)
#'
#' @return \code{flextable} of patient characteristics table
#'
#' @section Version control:
#' \itemize{
#'  * Created - ?
#'  * Updated - \itemize{
#'  * 11/18/2019 (VL) - updated to include dependent source codes and libraries; removed patient_char_table_groupcomp()
#'  function (currently: in its own script outside the Functions>DM folder since it hasn't been tested properly)
#'  * 06/15/2020 (RH) - fix bug when using test = T
#'    }
#' }
#'
#' @family Tables
#' @seealso
#' \itemize{
#' * \code{\link[gdxBiostats1]{genoCharTable}} for the Genomic Characteristics Table
#' }
#'
#' @examples
#' \dontrun{
#' data(infert)
#' tab.infert <- patient_char_table(infert, var = c('age', 'education', 'parity', 'induced', 'case'),
#'                                  cat.id = 2:5, cont.summ.type =  c('medMinMax', 'IQRQ1Q3'))
#' tab.infert2 <- patient_char_table(infert, var = c('education', 'parity', 'induced'),
#'                                   cat.id = 1:3, group.var = 'case', test = TRUE,
#'                                   cont.summ.type =  c('medMinMax', 'IQRQ1Q3'))
#' tab.infert3 <- patient_char_table(infert, var = c('education', 'parity', 'induced'),
#'                                   cat.id = 1:3, group.var = 'case', test = TRUE,
#'                                   cont.summ.type =  c('medMinMax', 'IQRQ1Q3'))
#'
#'
#' #create patient characteristics table displaying age, education, parity,
#' #induced, and case/control status for infertility
#'
#' # convert to flex table, while ordering and renaming variables as such:
#' # 1) Infertility (case), 2) Count (parity), 3) #Prior induced abortions (induced),
#' # 4) Age (age), 5) Education (years) (education)
#'
#' Ftab.infert <- patient_char_table_flex(tab.infert, pmerge = FALSE,
#'                                        new.names = c('No. patients (%)', 'Age', 'Education (years)', 'Count',
#'                                                      '# Prior Induced', 'Infertility'),
#'                                        header.names = c("Variables", "Study Cohort"),
#'                                        zebra.colors = c('#109ad644', '#d6c37444'))
#' # %>% setFlexTableWidths(width = rep(1.5, ncol(tab.infert))) # for ReporteRs
#' # %>% width(j = 1:ncol(df), width = rep(2, ncol(tab.infert)) # for flextable
#'
#' Ftab.infert2 <- patient_char_table_flex(tab.infert2,
#'                                         new.names = c('No. patients (%)', 'Age', 'Education (years)', 'Count'),
#'                                         header.names = c("Variables", "Infertility: No", "Infertility: Yes", "p value"),
#'                                         zebra.colors = c('grey88', 'white'))
#'
#' #debugging help:
#' #rm(df, var, cat.id, group.var, cat.perc, cont.dig, cat.useNA,
#' #  cont.summ.type, test, cont.test.type, cat.test.type, summ.cat,
#' #  summ.cont, switch, type, test.cat, test.cont, cont.vars, order.list,
#' #  temp, dup.ix, n.var, df.cat, df.cont, cat.vars, header.ix, unknown.lab,
#' #  out, ix, out2, ix.list, var.order, check, id.var, n, row.df, res.ix, i)
#' }
#'
#' @rdname patientCharTable
#' @export
#'

"patientCharTable" <- function(df, var = NULL, cat.id = NULL, group.var = NULL,
                               cat.perc = ".1f", cont.dig = ".3g",
                               cat.useNA = TRUE, cont.useNA = TRUE, unknown.lab = "Unavailable",
                               cont.summ.type = c("meanSD", "medMinMax", "medQ1Q3", "IQRQ1Q3"),
                               time.summ.type = c("medMinMax", "medQ1Q3", "IQRQ1Q3"),
                               test = FALSE, cont.test.type = c("oneway", "kruskal", "wilcox"),
                               cat.test.type = c("Approx", "Fisher"),
                               mult.comp = FALSE, remove.single.level = FALSE, remove.long.cat = TRUE){
  library(tidyr)
  library(dplyr)
  switch <- FALSE
  if(getOption("stringsAsFactors")){
    switch <- TRUE
    options(stringsAsFactors = FALSE)
  }
  n.var <- length(var)
  is.date <- function(x) inherits(x, "Date")

  ## helper function for summarizing a categorical variable x
  ## by default will output like N (%) for each level of the variable
  summ.cat <- function(x, na.opt){
    na.opt <- ifelse(na.opt, "ifany", "no")
    ns <- table(x, useNA = na.opt)
    pcts <- 100*ns/length(x)
    levs <- names(ns)
    data.frame(X = levs,
               Summ = sprintf(paste0("%d (%", cat.perc, ")"), ns, pcts))
  }

  ## helper function for summarizing a continuous variable x
  ## options for output: mean (SD), median (min, max), median (Q1, Q3)
  summ.cont <- function(x, which = c("meanSD", "medMinMax", "medQ1Q3", "IQRQ1Q3")){
    n <- length(which(is.na(x)))
    x <- x[!is.na(x)]
    if(length(x) != 0){
      mn <- mean(x); md <- median(x); sd1 <- sd(x); m0 <- min(x); m100 <- max(x)
      q1 <- quantile(as.numeric(x), 0.25); q3 <- quantile(as.numeric(x), 0.75)
      data <- data.frame(x = NA, Summ = NA)
      stat.type <- match.arg(which, c("meanSD", "medMinMax", "medQ1Q3", "IQRQ1Q3"), several.ok = T)
      if(cont.useNA){
        out <- sapply(stat.type, switch,
                      meanSD = ifelse(n != 0,
                                      sprintf(paste0("%", cont.dig, " (%", cont.dig, "; NA=%.0f)"),
                                              mn, sd1, n),
                                      sprintf(paste0("%", cont.dig, " (%", cont.dig, ")"),
                                              mn, sd1)),
                      medMinMax = ifelse(is.date(x),
                                         sprintf("%s (%s, %s)", as.character(md), as.character(m0), as.character(m100)),
                                         ifelse(n != 0,
                                                sprintf(paste0("%", cont.dig, " (%", cont.dig, ", %", cont.dig, "; NA=%.0f)"),
                                                        md, m0, m100, n),
                                                sprintf(paste0("%", cont.dig, " (%", cont.dig, ", %", cont.dig, ")"),
                                                        md, m0, m100))),
                      medQ1Q3 = ifelse(is.date(x),
                                       sprintf("%s (%s, %s)", as.character(md),
                                               as.character(as.Date(q1, origin = "1970-01-01")),
                                               as.character(as.Date(q3, origin = "1970-01-01"))),
                                       ifelse(n != 0,
                                              sprintf(paste0("%", cont.dig, " (%", cont.dig, ", %", cont.dig, "; NA=%.0f)"),
                                                      md, q1, q3, n),
                                              sprintf(paste0("%", cont.dig, " (%", cont.dig, ", %", cont.dig, ")"),
                                                      md, q1, q3))),
                      IQRQ1Q3 = ifelse(is.date(x),
                                       sprintf("%s - %s", as.character(as.Date(q1, origin = "1970-01-01")),
                                               as.character(as.Date(q3, origin = "1970-01-01"))),
                                       ifelse(n != 0,
                                              sprintf(paste0("%", cont.dig, " - %", cont.dig, "; NA=%.0f"),
                                                      q1, q3, n),
                                              sprintf(paste0("%", cont.dig, " - %", cont.dig),
                                                      q1, q3))))

      }else{
        out <- sapply(stat.type, switch,
                      meanSD = sprintf(paste0("%", cont.dig, " (%", cont.dig, ")"), mn, sd1),
                      medMinMax = sprintf(paste0("%", cont.dig, " (%", cont.dig, ", %", cont.dig, ")"), md, m0, m100),
                      medQ1Q3 = sprintf(paste0("%", cont.dig, " (%", cont.dig, ", %", cont.dig, ")"), md, q1, q3),
                      IQRQ1Q3 = sprintf(paste0("%", cont.dig, " - %", cont.dig ), q1, q3))
      }

    }else{
      stat.type <- match.arg(which, c("meanSD", "medMinMax", "medQ1Q3", "IQRQ1Q3"), several.ok = T)
      out <- sapply(stat.type, switch,
                    meanSD = "Unavailable",
                    medMinMax = "Unavailable",
                    medQ1Q3 = "Unavailable",
                    IQRQ1Q3 = "Unavailable")
    }
    lab <- sapply(stat.type, switch,
                  meanSD = "Mean (SD)",
                  medMinMax = "Median (Range)",
                  medQ1Q3 = "Median (Q1, Q3)",
                  IQRQ1Q3 = "IQR (Q1, Q3)")
    data.frame(X = lab, Summ = out)
  }

  test.cat <- function(x, y, which = c("Approx", "Fisher")){


    # ap <- chisq.test(x, y); fp <- try(fisher.test(x, y))
    test.type <- match.arg(which, c("Approx", "Fisher"))
    if(length(unique(na.omit(x))) < 2){
      out <- NA
    }else{
      out <- switch(test.type,
                    Approx = chisq.test(x, y)$p.value,
                    Fisher = try(fisher.test(x, y)$p.value))
      if(is(out, "try-error")){
        print("Fisher test sample size too large: switching to Approximate test")
        out <- chisq.test(x, y)$p.value
      }
    }

    if(mult.comp) out <- min(out*n.var, 1)

    # out <- sapply(test.type, switch,
    #               Approx = ifelse(ap$p.value < 0.001, "<0.001", sprintf("%.3f", ap$p.value)),
    #               Fisher = ifelse(fp$p.value < 0.001, "<0.001", sprintf("%.3f", fp$p.value)))
    out
  }

  test.cont <- function(x, y, which = c("oneway", "kruskal", "Wilcox")){
    test.type <- match.arg(which, c("oneway", "kruskal", "Wilcox"))
    # op <- summary(aov(x~y))[[1]][1, "Pr(>F)"]; kp <- kruskal.test(x ~ y)$p.value
    out <- sapply(test.type, switch,
                  oneway = summary(aov(x~y))[[1]][1, "Pr(>F)"],
                  kruskal = {y <- factor(y)
                  kruskal.test(x ~ y)$p.value},
                  wilcox = wilcox.test(x~factor(y)))

    if(mult.comp) out <- min(out*n.var, 1)

    # out <- sapply(test.type, switch,
    #               oneway = ifelse(op < 0.001, "<0.001", sprintf("%.3f", op)),
    #               kruskal = ifelse(kp < 0.001, "<0.001", sprintf("%.3f", kp)))
    out
  }

  ## wrapper part of the function
  stopifnot(!is.null(var))
  stopifnot(is.logical(cat.useNA))
  stopifnot(is.logical(mult.comp))
  stopifnot(length(cat.id) <= length(var))
  if(!is.null(cat.id)){
    stopifnot(max(cat.id) <= length(var))
  }
  if(any(c("var", "val", "group.var") %in% c(var, group.var))){
    stop("Please change the input data variable names to avoid conflict")
  }
  type <- match.arg(cont.summ.type, c("meanSD", "medMinMax", "medQ1Q3", "IQRQ1Q3"), several.ok = T)
  time.type <- match.arg(time.summ.type, c("medMinMax", "medQ1Q3", "IQRQ1Q3"), several.ok = T)
  cat.test.type <- match.arg(cat.test.type, c("Approx", "Fisher"))
  cont.test.type <- match.arg(cont.test.type, c("oneway", "kruskal" ,"wilcox"))
  if(!is.null(cat.id)){
    cat.vars <- var[cat.id]
    cont.vars <- var[-cat.id]
  } else{
    cat.vars <-  NULL
    cont.vars <- var
  }

  ## QC of categorical variables:
  if(!is.null(cat.vars)){
    # 1. remove categorical variables that are too unique (too many rows),
    # namely if the highest frequency of one level is less than twice the number of grouping var levels:
    n.tabmax <- sapply(cat.vars, function(x) max(table(df[[x]])))
    n.distinct <- sapply(cat.vars, function(x) n_distinct(na.omit(df[[x]])))
    if(!is.null(group.var)){
      out.id <- which(n.tabmax < 2*n_distinct(df[[group.var]]))
    } else{
      out.id <- which(n.distinct > nrow(df)/10)
    }

    if(length(out.id) > 0 & remove.long.cat){
      cat("\n The following categorical variables will be removed since there are too many unique levels:\n")
      df.print <- data.frame(variable = names(out.id),
                             N.unique.values = n.distinct[names(out.id)], row.names = NULL)
      print(df.print)
    } else out.id <- NULL
    # 2. remove single-level variables:
    out.id2 <- which(n.distinct <= 1)
    if(length(out.id2) > 0 & remove.single.level){
      cat("\n The following categorical variables will be removed for lack of unique levels (1 or fewer non-NA value):\n")
      df.print <- data.frame(variable = names(out.id2),
                             unique.value = sapply(names(out.id2), function(x) names(table(df[[x]], useNA = "ifany"))[1]),
                             row.names = NULL)
      print(df.print)
    } else out.id2 <- NULL
    cat.out <- cat.vars[c(out.id, out.id2)]
    var <- setdiff(var, cat.out)
    cat.vars <- setdiff(cat.vars, cat.out)

  }

  df <- df[c(var, group.var)]
  if(any(sapply(df, is.date))){
    time.vars <- cont.vars[sapply(df[cont.vars], is.date)]
    cont.vars <- cont.vars[-which(sapply(df[cont.vars], is.date))]
  }else{
    time.vars <- NULL
  }


  if(!is.null(group.var)){
    df$group.var <- df[[group.var]]
    df[[group.var]] <- NULL
    if(length(cat.vars) != 0){
      stopifnot(all(cat.vars %in% names(df)))
      ## replace NA with "99, missing" for the categorical variables for formatting purposes
      # removed this line as this breaks down for factors
      temp <- df
      # if(cat.useNA) if(any(is.na(temp[cat.vars]))) temp[cat.vars][is.na(temp[cat.vars])] <- "99, missing"
      df.cat <- gather(temp, "var" , "val", one_of(cat.vars)) %>%
        group_by(var, group.var) %>%
        do(summ.cat(.$val, na.opt = cat.useNA)) %>%
        spread(group.var, Summ)

      ## re-order levels:
      order.list <- lapply(temp[cat.vars], function(x){
        out <- levels(factor(x))
        if(cat.useNA & any(is.na(x))) out <- c(out, NA)
        out
      })

      df.cat <- df.cat %>% do(.[match(order.list[[unique(.$var)]], .$X),])

      ## then re-order groups according to cat.vars
      # df.cat <- df.cat[unlist(attr(df.cat, "indices")[match(cat.vars, attr(df.cat, "labels")$var)]) + 1,]
      df.cat <- df.cat[unlist(attr(df.cat, "groups")$.rows[match(cat.vars, attr(df.cat, "groups")$var)]),]
      rm(order.list, temp)

      if(test){
        df.cat <- df.cat %>% as.data.frame()
        df.cat$p <- NA
        for(i in 1:length(cat.vars)){ ## for each variable

          df.cat[df.cat$var == cat.vars[i], ]$p <-
            tryCatch(test.cat(df[[cat.vars[i]]], df$group.var, which = cat.test.type),
                     error = function(e) return(NA))
        }
      }
    } else df.cat <- NULL
    if(length(cont.vars) != 0){
      stopifnot(all(cont.vars %in% names(df)))
      df.cont <- gather(df, "var" , "val", one_of(cont.vars)) %>%
        group_by(var, group.var) %>%
        do(summ.cont(.$val, which = type)) %>%
        spread(group.var, Summ)
      if(test){
        df.cont <- df.cont %>% as.data.frame()
        df.cont$p <- NA
        for(i in 1:length(cont.vars)){
          df.cont[df.cont$var == cont.vars[i], ]$p <-
            tryCatch(test.cont(df[[cont.vars[i]]], df$group.var, which = cont.test.type),
                     error = function(e)return(NA))
        }
      }
    } else df.cont <- NULL
    if(length(time.vars) != 0){
      stopifnot(all(time.vars %in% names(df)))
      df.time <- gather(df, "var", "val", one_of(time.vars)) %>%
        group_by(var, group.var) %>%
        do(summ.cont(.$val, which = time.type)) %>%
        spread(group.var, Summ)
      if(test){
        df.time$p <- NA
      }
    } else df.time <- NULL
    out <- rbind(df.cat, df.cont, df.time)
  }else{
    if(length(cat.vars) != 0){
      stopifnot(all(cat.vars %in% names(df)))
      ## replace NA with "99, missing" for the categorical variables for formatting purposes
      temp <- df
      # if(cat.useNA) if(any(is.na(temp[cat.vars]))) temp[cat.vars][is.na(temp[cat.vars])] <- "99, missing"
      df.cat <- gather(temp, "var" , "val", one_of(cat.vars)) %>%
        group_by(var) %>%
        do(summ.cat(.$val, na.opt = cat.useNA))
      # order <- unlist(sapply(temp[cat.vars], function(x) levels(factor(x))))
      # df.cat <- df.cat[pmatch(order, df.cat$X), ]
      ## This has been copied from above(grouped section)... hopefully it"s generalizable:
      order.list <- lapply(temp[cat.vars], function(x){
        out <- levels(factor(x))
        if(cat.useNA & any(is.na(x))) out <- c(out, NA)
        out
      })
      df.cat <- df.cat %>% do(data.frame(.[match(order.list[[unique(.$var)]], .$X),]))

      rm(order.list, temp)

    } else df.cat <- NULL
    if(length(cont.vars) != 0){
      stopifnot(all(cont.vars %in% names(df)))
      df.cont <- gather(df, "var" , "val", one_of(cont.vars)) %>%
        group_by(var) %>%
        do(summ.cont(.$val, which = type))
    } else df.cont <- NULL
    if(length(time.vars) != 0){
      stopifnot(all(time.vars %in% names(df)))
      df.time <- gather(df, "var", "val", one_of(time.vars)) %>%
        group_by(var) %>%
        do(summ.cont(.$val, which = time.type))
    } else df.time <- NULL
    out <- rbind(df.cat, df.cont, df.time)
  }

  ## here"s the nitpicky formatting portion:
  n.var <- length(c(cat.vars, cont.vars, time.vars))
  dup.ix <- duplicated(out$var)
  header.ix <- which(!dup.ix) + 0:(n.var - 1)
  res.ix <- setdiff(1:(nrow(out) + n.var), header.ix)
  out2 <- data.frame(Variables = rep(NA, n.var + nrow(out)),
                     Summ = matrix(NA, n.var + nrow(out), length(unique(df$group.var))))
  out2$Variables[header.ix] <- as.character(out$var[!dup.ix])
  out2$Variables[res.ix] <- out$X
  if(is.null(group.var)){
    out2$Summ[res.ix] <- out$Summ
  } else{
    for(i in 1:length(unique(df$group.var))){
      out2[res.ix, i+1] <- out[, i+2]
    }
    out2$p[res.ix] <- out$p
  }
  out2$Variables[is.na(out2$Variables)] <- unknown.lab
  # out2$Variables[out2$Variables == "99, missing"] <- unknown.lab


  ## Adding total counts row to the top of the summary table
  if(is.null(group.var)){
    row.df <- data.frame(Variables = "Total",
                         Summ = sprintf(sprintf("%%d (%%%s)", cat.perc), nrow(df), 100))
    out2 <- rbind(row.df, out2)
  } else{
    row.df <- data.frame(Variables = "Total",
                         Summ = sprintf(sprintf("%%d (%%%s)", cat.perc),
                                        table(df$group.var, useNA = "ifany"),
                                        100*prop.table(table(df$group.var, useNA = "ifany"))),
                         Gp = sprintf("Summ.%d", 1:n_distinct(df$group.var))) %>%
      spread(Gp, Summ)
    row.df <- subset(row.df, select = c("Variables", sprintf("Summ.%d", 1:n_distinct(df$group.var))))
    if(test) row.df$p <- NA

    out2 <- rbind(row.df, out2)
    colnames(out2)[2:ncol(out2)] <- colnames(out)[3:ncol(out)]
  }

  ## re-order rows of table:
  ix = c(1, header.ix+1)
  n <- length(ix)
  id.var <- out2$Variables[ix]

  check <- c("Total", var)
  var.order <- match(check, id.var)

  ix.list <- mapply(function(x, y) x:y, ix, c(ix[2:n] - 1, nrow(out2)), SIMPLIFY = F)
  out2 <- do.call("rbind", lapply(ix.list[var.order], function(x) out2[x,]))

  ## format p-values:
  if(test){
    out2$p <- ifelse(out2$p < 0.001, "<0.001", sprintf("%.3f", out2$p))
    out2$p[1] <- " "
  }

  # id <- c(1, grep(paste(var, collapse = "|"), out2$Variables))
  # n <- na.omit(lead(c(id, nrow(out2)+1))) - id
  # out2$pcol <- rep(0:length(var), n, each = T)
  if(switch) options(stringsAsFactors = TRUE)
  return(out2)

}




#' @rdname patientCharTable
#' @export
#'

"patientCharTableFlex" <- function(patientTable, pmerge = TRUE,
                                    new.names = NULL, size = 10,
                                    header.names = NULL,
                                    zebra = TRUE,
                                    zebra.colors = c("#EEEEEE", "white"),
                                    newRenameList = NULL,
                                    borderWidth = 1){
  library(flextable)
  library(officer)

  "reHeader" <- function(old.header, new.header){
    myString <- do.call(function(i, j) paste(shQuote(i, "sh"), "=", shQuote(j, "sh"), ","), list(old.header, new.header))
    myString <- capture.output(cat(myString))
    return(gsub(",$", " ", myString))
  }

  patientTable$Variables <- as.character(patientTable$Variables)
  if(ncol(patientTable) > 2) {
    new.ix <- c(1, which(apply(patientTable[, 2:ncol(patientTable)], 1, function(x) all(is.na(x))) ==T))
  } else{
    new.ix <- c(1, which(is.na(patientTable[, 2])))
  }
  other.ix <- (1:nrow(patientTable))[-new.ix]

  # change all NAs that are not in header columns to avoid font size inconsistency in table output
  patientTable[other.ix, 2:ncol(patientTable)][is.na(patientTable[other.ix, 2:ncol(patientTable)])] <- " "
  patientTable[new.ix, 2:ncol(patientTable)][is.na(patientTable[new.ix, 2:ncol(patientTable)])] <- "  "

  if(!is.null(new.names)){
    patientTable$Variables[new.ix] <- new.names
  } else patientTable$Variables[new.ix] <- renameGdx(patientTable$Variables[new.ix], newList = newRenameList)


  if("flextable" %in% rownames(installed.packages())){

    library(flextable)
    size <- 1.2*size # a bit bigger font

    Ftab <- patientTable %>% regulartable() %>%
      align(align = "center", part = "all") %>%
      align(align = "left", j = 1) %>%
      flextable::font(fontname = "Times New Roman", part = "all") %>%
      fontsize(size = size, part = "all") %>%
      fontsize(size = 1.2*size, part = "header") %>%
      padding(padding.left = 20,
              i = other.ix, j = 1) %>%
      bold(part = "header") %>%
      bold(i = new.ix, j = 1) %>%
      align(align = "left", j = 1, part = "header") %>%
      border_inner_h(border = fp_border(color = NA, width = 1), part = "body") %>%
      autofit() %>%
      height(height = 0.15)

    if(pmerge){
      Ftab <- Ftab %>%
        merge_v(j = c("p"))
    }

    if(!is.null(header.names)){
      eval(parse(text = paste0("Ftab <- set_header_labels(Ftab, ",
                               reHeader(colnames(patientTable), header.names),
                               ")")))
    }

    if(zebra){
      if(pmerge){
        new.list <- mapply(function(x, y) x:y, new.ix, c(new.ix[2:length(new.ix)] - 1, nrow(patientTable)), SIMPLIFY = F)

        odds <- unlist(new.list[seq(1, length(new.ix), 2)])
        evens <- unlist(new.list[seq(2, length(new.ix), 2)])

        Ftab <- Ftab %>%
          bg(i = odds, bg = zebra.colors[2]) %>%
          bg(i = evens, bg = zebra.colors[1])

      } else{

        odds <- seq(1, nrow(patientTable), by = 2)
        evens <- odds + 1
        evens <- evens[!evens > nrow(patientTable)]

        Ftab <- Ftab %>%
          bg(i = odds, bg = zebra.colors[2]) %>%
          bg(i = evens, bg = zebra.colors[1])

      }

    }

  } else{
    if("ReporteRs" %in% rownames(installed.packages())){

      library(ReporteRs)

      ## format Flex Table
      if(pmerge){
        if(!is.null(header.names)){
          Ftab <- FlexTable(patientTable, header.columns = FALSE,
                            header.text.props = textProperties(font.size = 1.2*size, font.weight = "bold"),
                            header.par.props = parProperties(text.align = "center"),
                            body.text.props = textProperties(font.size = size),
                            body.par.props = parProperties(text.align = "center")) %>%
            spanFlexTableRows(j = "p", runs = patientTable$p) %>%
            addHeaderRow(header.names, colspan = rep(1, ncol(patientTable)),
                         text.properties = textProperties(font.size = 1.2*size, font.weight = "bold"),
                         par.properties = parProperties(text.align = "center"))
        } else{
          Ftab <- FlexTable(patientTable,
                            header.text.props = textProperties(font.size = 1.2*size, font.weight = "bold"),
                            header.par.props = parProperties(text.align = "center"),
                            body.text.props = textProperties(font.size = size),
                            body.par.props = parProperties(text.align = "center")) %>%
            spanFlexTableRows(j = "p", runs = patientTable$p)
        }
      } else{
        if(!is.null(header.names)){
          Ftab <- FlexTable(patientTable, header.columns = FALSE,
                            header.text.props = textProperties(font.size = 1.2*size, font.weight = "bold"),
                            header.par.props = parProperties(text.align = "center"),
                            body.text.props = textProperties(font.size = size),
                            body.par.props = parProperties(text.align = "center")) %>%
            addHeaderRow(header.names, colspan = rep(1, ncol(patientTable)),
                         text.properties = textProperties(font.size = 1.2*size, font.weight = "bold"),
                         par.properties = parProperties(text.align = "center"))
        } else{
          Ftab <- FlexTable(patientTable,
                            header.text.props = textProperties(font.size = 1.2*size, font.weight = "bold"),
                            header.par.props = parProperties(text.align = "center"),
                            body.text.props = textProperties(font.size = size),
                            body.par.props = parProperties(text.align = "center"))
        }
      }

      Ftab <- setFlexTableBorders(Ftab,
                                  inner.vertical = borderNone(), inner.horizontal = borderNone(),
                                  outer.vertical = borderNone(), borderProperties(width = borderWidth), footer = T) %>%
        spanFlexTableColumns(i = new.ix[-1], from = 1, to = ncol(patientTable))

      Ftab[new.ix, 1] <- textProperties(font.weight = "bold", font.size = 1.2*size)
      Ftab[, 1] <- parProperties(text.align = "left", padding.left = 20)
      Ftab[, 1, to = "header"] <- parLeft()
      Ftab[new.ix, 1] <- parLeft()

      new.list <- mapply(function(x, y) x:y, new.ix, c(new.ix[2:length(new.ix)] - 1, nrow(patientTable)), SIMPLIFY = F)

      odds <- unlist(new.list[seq(1, length(new.ix), 2)])
      evens <- unlist(new.list[seq(2, length(new.ix), 2)])

      if(zebra){
        if(pmerge){
          Ftab <- setRowsColors(Ftab, i = odds, colors = zebra.colors[1])
          Ftab <- setRowsColors(Ftab, i = evens, colors = zebra.colors[2])
        } else{
          Ftab <- setZebraStyle(Ftab, odd = zebra.colors[1], even = zebra.colors[2])
        }
      }

    } else{
      print("Install either ReporteRs or flextable package")
    }
  }

  Ftab %>% fix_border_issues()
}
